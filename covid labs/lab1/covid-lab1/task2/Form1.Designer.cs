﻿namespace task2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNotepad = new System.Windows.Forms.Button();
            this.btnPowerPoint = new System.Windows.Forms.Button();
            this.btnWordOffice = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNotepad
            // 
            this.btnNotepad.Location = new System.Drawing.Point(95, 35);
            this.btnNotepad.Name = "btnNotepad";
            this.btnNotepad.Size = new System.Drawing.Size(258, 60);
            this.btnNotepad.TabIndex = 0;
            this.btnNotepad.Text = "Notepad";
            this.btnNotepad.UseVisualStyleBackColor = true;
            this.btnNotepad.Click += new System.EventHandler(this.btnNotepad_Click);
            // 
            // btnPowerPoint
            // 
            this.btnPowerPoint.Location = new System.Drawing.Point(95, 133);
            this.btnPowerPoint.Name = "btnPowerPoint";
            this.btnPowerPoint.Size = new System.Drawing.Size(258, 60);
            this.btnPowerPoint.TabIndex = 1;
            this.btnPowerPoint.Text = "Power Point";
            this.btnPowerPoint.UseVisualStyleBackColor = true;
            // 
            // btnWordOffice
            // 
            this.btnWordOffice.Location = new System.Drawing.Point(95, 229);
            this.btnWordOffice.Name = "btnWordOffice";
            this.btnWordOffice.Size = new System.Drawing.Size(258, 60);
            this.btnWordOffice.TabIndex = 2;
            this.btnWordOffice.Text = "MS Word";
            this.btnWordOffice.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnWordOffice);
            this.Controls.Add(this.btnPowerPoint);
            this.Controls.Add(this.btnNotepad);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Button btnNotepad;
        private System.Windows.Forms.Button btnPowerPoint;
        private System.Windows.Forms.Button btnWordOffice;

        #endregion
    }
}