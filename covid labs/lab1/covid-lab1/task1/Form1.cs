﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task1
{
    public partial class Form1 : Form
    {
        Person[] person;

        public Form1()
        {
            InitializeComponent();
            person = new Person[5];
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = person;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < person.Length; i++)
            {
                person[i] = new Person();
            }

            for (int i = 0; i < person.Length; i++)
            {
                person[i].FirstName1 = "A" + i;
                person[i].LastName1 = "B" + i;
                person[i].City1 = "C" + i;
            }
        }
    }
}