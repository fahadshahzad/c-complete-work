﻿using System;

namespace task1
{
    public class Person
    {
        private String FirstName;
        private String LastName;
        private String City;

        public Person()
        {
        }

        public Person(string firstName, string lastName, string city)
        {
            FirstName = firstName;
            LastName = lastName;
            City = city;
        }

        public string FirstName1
        {
            get => FirstName;
            set => FirstName = value;
        }

        public string LastName1
        {
            get => LastName;
            set => LastName = value;
        }

        public string City1
        {
            get => City;
            set => City = value;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}