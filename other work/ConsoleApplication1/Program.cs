﻿using System;

namespace ConsoleApplication1
{
    /// <summary>
    ///    Program class
    ///    project start from main method of this class
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main method
        /// Entry point of project
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}