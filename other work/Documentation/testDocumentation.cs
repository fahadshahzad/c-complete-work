using System;

namespace simple
{
	///	<summary>
	///		Program Class
	///		Project starts from Main method of this classs
	///	</summary>
    class Program
    {
	///	<summary>
	///		Main Method
	///		Entry point of Project
	///	</summary>
        static void Main(string[] args)
        {
            string[] names = {"first name", "second name"};
			foreach(string name in names)
				Console.WriteLine(name);
        }
    }
}