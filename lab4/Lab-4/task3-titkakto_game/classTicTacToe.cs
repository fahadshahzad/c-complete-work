﻿namespace task3_titkakto_game
{
    public class classTicTacToe
    {
        private bool _toggleValue = true;
        private string _userOne = "0";
        private string _userTwo = "X";

        public bool ToggleValue
        {
            get => _toggleValue;
            set => _toggleValue = value;
        }

        public string UserOne
        {
            get => _userOne;
        }

        public string UserTwo
        {
            get => _userTwo;
        }
    }
}