﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task3_titkakto_game
{
    public partial class Form1 : Form
    {
        private classTicTacToe obj = new classTicTacToe();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOne_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnOne.Text = obj.UserOne;
            }
            else
            {
                this.btnOne.Text = obj.UserTwo;
            }
        }

        private void btnTwo_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnTwo.Text = obj.UserOne;
            }
            else
            {
                this.btnTwo.Text = obj.UserTwo;
            }
        }

        private void btnThree_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnThree.Text = obj.UserOne;
            }
            else
            {
                this.btnThree.Text = obj.UserTwo;
            }
        }

        private void btnFour_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnFour.Text = obj.UserOne;
            }
            else
            {
                this.btnFour.Text = obj.UserTwo;
            }
        }

        private void btnFive_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnFive.Text = obj.UserOne;
            }
            else
            {
                this.btnFive.Text = obj.UserTwo;
            }
        }

        private void btnSix_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnSix.Text = obj.UserOne;
            }
            else
            {
                this.btnSix.Text = obj.UserTwo;
            }
        }

        private void btnSeven_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnSeven.Text = obj.UserOne;
            }
            else
            {
                this.btnSeven.Text = obj.UserTwo;
            }
        }

        private void btnEight_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnEight.Text = obj.UserOne;
            }
            else
            {
                this.btnEight.Text = obj.UserTwo;
            }
        }

        private void btnNine_Click(object sender, EventArgs e)
        {
            if (obj.ToggleValue)
            {
                this.btnNine.Text = obj.UserOne;
            }
            else
            {
                this.btnNine.Text = obj.UserTwo;
            }
        }
    }
}