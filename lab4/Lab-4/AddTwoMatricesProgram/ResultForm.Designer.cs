﻿using System.ComponentModel;

namespace AddTwoMatricesProgram
{
    partial class ResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.element02 = new System.Windows.Forms.Label();
            this.element01 = new System.Windows.Forms.Label();
            this.element11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.element00 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (0)))), ((int) (((byte) (0)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(242, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(274, 51);
            this.button1.TabIndex = 0;
            this.button1.Text = "End Application";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.element02);
            this.groupBox1.Controls.Add(this.element01);
            this.groupBox1.Controls.Add(this.element11);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.element00);
            this.groupBox1.Location = new System.Drawing.Point(75, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(561, 310);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sum of Matrix1 and Matrix2";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(312, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "element22";
            // 
            // element02
            // 
            this.element02.Location = new System.Drawing.Point(312, 60);
            this.element02.Name = "element02";
            this.element02.Size = new System.Drawing.Size(129, 29);
            this.element02.TabIndex = 4;
            this.element02.Text = "element02";
            // 
            // element01
            // 
            this.element01.Location = new System.Drawing.Point(216, 60);
            this.element01.Name = "element01";
            this.element01.Size = new System.Drawing.Size(129, 29);
            this.element01.TabIndex = 3;
            this.element01.Text = "element01";
            // 
            // element11
            // 
            this.element11.Location = new System.Drawing.Point(85, 108);
            this.element11.Name = "element11";
            this.element11.Size = new System.Drawing.Size(129, 29);
            this.element11.TabIndex = 2;
            this.element11.Text = "element20";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(216, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "element21";
            // 
            // element00
            // 
            this.element00.Location = new System.Drawing.Point(85, 60);
            this.element00.Name = "element00";
            this.element00.Size = new System.Drawing.Size(129, 29);
            this.element00.TabIndex = 0;
            this.element00.Text = "element00";
            // 
            // ResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Name = "ResultForm";
            this.Text = "ResultForm";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Label element00;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label element11;
        private System.Windows.Forms.Label element01;
        private System.Windows.Forms.Label element02;
        private System.Windows.Forms.Label label6;

        private System.Windows.Forms.GroupBox groupBox1;

        private System.Windows.Forms.Button button1;

        #endregion
    }
}