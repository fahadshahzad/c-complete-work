﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddTwoMatricesProgram
{
    public partial class Form1 : Form
    {
        private Form2 _form2 = new Form2();
        private static int[,] _matrix1;
        public Form1()
        {
            _matrix1 = new int[2, 3];
            InitializeComponent();
        }

        public static int[,] Matrix1
        {
            get => _matrix1;
            set => _matrix1 = value;
        }

        public int[,] GetMatrixValues()
        {
            return _matrix1;
        }

        public TextBox Element02
        {
            get => element02;
            set => element02 = value;
        }

        public TextBox Element12
        {
            get => element12;
            set => element12 = value;
        }

        public TextBox Element00
        {
            set => element00 = value;
            get => element00;
        }

        public TextBox Element11
        {
            get => element11;
            set => element11 = value;
        }

        public TextBox Element10
        {
            get => element10;
            set => element10 = value;
        }

        public TextBox Element01
        {
            get => element01;
            set => element01 = value;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void submitMatrix1_Click(object sender, EventArgs e)
        {
            _matrix1[0, 0] = int.Parse(this.element00.Text);
            _matrix1[0, 1] = int.Parse(this.element01.Text);
            _matrix1[0, 2] = int.Parse(this.element02.Text);
            _matrix1[1, 0] = int.Parse(this.element10.Text);
            _matrix1[1, 1] = int.Parse(this.element11.Text);
            _matrix1[1, 2] = int.Parse(this.element12.Text);
            this.Hide();
            _form2.ShowDialog();
        }
    }
}
