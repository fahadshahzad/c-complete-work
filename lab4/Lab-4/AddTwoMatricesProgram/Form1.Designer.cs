﻿namespace AddTwoMatricesProgram
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.submitMatrix1 = new System.Windows.Forms.Button();
            this.element00 = new System.Windows.Forms.TextBox();
            this.element11 = new System.Windows.Forms.TextBox();
            this.element10 = new System.Windows.Forms.TextBox();
            this.element01 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.element12 = new System.Windows.Forms.TextBox();
            this.element02 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // submitMatrix1
            // 
            this.submitMatrix1.Location = new System.Drawing.Point(212, 199);
            this.submitMatrix1.Name = "submitMatrix1";
            this.submitMatrix1.Size = new System.Drawing.Size(261, 43);
            this.submitMatrix1.TabIndex = 0;
            this.submitMatrix1.Text = "Submit";
            this.submitMatrix1.UseVisualStyleBackColor = true;
            this.submitMatrix1.Click += new System.EventHandler(this.submitMatrix1_Click);
            // 
            // element00
            // 
            this.element00.Location = new System.Drawing.Point(100, 74);
            this.element00.Name = "element00";
            this.element00.Size = new System.Drawing.Size(163, 20);
            this.element00.TabIndex = 1;
            // 
            // element11
            // 
            this.element11.Location = new System.Drawing.Point(342, 119);
            this.element11.Name = "element11";
            this.element11.Size = new System.Drawing.Size(163, 20);
            this.element11.TabIndex = 3;
            // 
            // element10
            // 
            this.element10.Location = new System.Drawing.Point(100, 119);
            this.element10.Name = "element10";
            this.element10.Size = new System.Drawing.Size(163, 20);
            this.element10.TabIndex = 5;
            // 
            // element01
            // 
            this.element01.Location = new System.Drawing.Point(342, 74);
            this.element01.Name = "element01";
            this.element01.Size = new System.Drawing.Size(163, 20);
            this.element01.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label1.Location = new System.Drawing.Point(230, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(275, 40);
            this.label1.TabIndex = 7;
            this.label1.Text = "Enter Matrix 1 Values";
            // 
            // element12
            // 
            this.element12.Location = new System.Drawing.Point(542, 119);
            this.element12.Name = "element12";
            this.element12.Size = new System.Drawing.Size(163, 20);
            this.element12.TabIndex = 8;
            // 
            // element02
            // 
            this.element02.Location = new System.Drawing.Point(542, 74);
            this.element02.Name = "element02";
            this.element02.Size = new System.Drawing.Size(163, 20);
            this.element02.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.element02);
            this.Controls.Add(this.element12);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.element01);
            this.Controls.Add(this.element10);
            this.Controls.Add(this.element11);
            this.Controls.Add(this.element00);
            this.Controls.Add(this.submitMatrix1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.TextBox element02;
        private System.Windows.Forms.TextBox element12;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.Button submitMatrix1;
        private System.Windows.Forms.TextBox element00;
        private System.Windows.Forms.TextBox element11;
        private System.Windows.Forms.TextBox element10;
        private System.Windows.Forms.TextBox element01;

        #endregion
    }
}

