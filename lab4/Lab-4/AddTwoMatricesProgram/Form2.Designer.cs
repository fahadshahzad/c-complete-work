﻿using System.ComponentModel;

namespace AddTwoMatricesProgram
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.matrix2 = new System.Windows.Forms.Label();
            this.element00 = new System.Windows.Forms.TextBox();
            this.element01 = new System.Windows.Forms.TextBox();
            this.element02 = new System.Windows.Forms.TextBox();
            this.element20 = new System.Windows.Forms.TextBox();
            this.element21 = new System.Windows.Forms.TextBox();
            this.element22 = new System.Windows.Forms.TextBox();
            this.submitMatrix2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // matrix2
            // 
            this.matrix2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.matrix2.Location = new System.Drawing.Point(295, 109);
            this.matrix2.Name = "matrix2";
            this.matrix2.Size = new System.Drawing.Size(275, 40);
            this.matrix2.TabIndex = 13;
            this.matrix2.Text = "Enter Matrix 2 Values";
            // 
            // element00
            // 
            this.element00.Location = new System.Drawing.Point(165, 174);
            this.element00.Name = "element00";
            this.element00.Size = new System.Drawing.Size(163, 20);
            this.element00.TabIndex = 9;
            // 
            // element01
            // 
            this.element01.Location = new System.Drawing.Point(392, 174);
            this.element01.Name = "element01";
            this.element01.Size = new System.Drawing.Size(163, 20);
            this.element01.TabIndex = 12;
            // 
            // element02
            // 
            this.element02.Location = new System.Drawing.Point(588, 174);
            this.element02.Name = "element02";
            this.element02.Size = new System.Drawing.Size(163, 20);
            this.element02.TabIndex = 14;
            // 
            // element20
            // 
            this.element20.Location = new System.Drawing.Point(165, 219);
            this.element20.Name = "element20";
            this.element20.Size = new System.Drawing.Size(163, 20);
            this.element20.TabIndex = 11;
            // 
            // element21
            // 
            this.element21.Location = new System.Drawing.Point(392, 219);
            this.element21.Name = "element21";
            this.element21.Size = new System.Drawing.Size(163, 20);
            this.element21.TabIndex = 15;
            // 
            // element22
            // 
            this.element22.Location = new System.Drawing.Point(588, 219);
            this.element22.Name = "element22";
            this.element22.Size = new System.Drawing.Size(163, 20);
            this.element22.TabIndex = 10;
            // 
            // submitMatrix2
            // 
            this.submitMatrix2.Location = new System.Drawing.Point(277, 299);
            this.submitMatrix2.Name = "submitMatrix2";
            this.submitMatrix2.Size = new System.Drawing.Size(261, 43);
            this.submitMatrix2.TabIndex = 8;
            this.submitMatrix2.Text = "Submit";
            this.submitMatrix2.UseVisualStyleBackColor = true;
            this.submitMatrix2.Click += new System.EventHandler(this.submitMatrix2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.matrix2);
            this.Controls.Add(this.element00);
            this.Controls.Add(this.element01);
            this.Controls.Add(this.element02);
            this.Controls.Add(this.element20);
            this.Controls.Add(this.element21);
            this.Controls.Add(this.element22);
            this.Controls.Add(this.submitMatrix2);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();
        }


        private System.Windows.Forms.TextBox element00;
        private System.Windows.Forms.TextBox element01;
        private System.Windows.Forms.TextBox element02;
        private System.Windows.Forms.TextBox element20;
        private System.Windows.Forms.TextBox element21;
        private System.Windows.Forms.TextBox element22;
        private System.Windows.Forms.Label matrix2;
        private System.Windows.Forms.Button submitMatrix2;

        #endregion
    }
}