﻿using System;
using System.Windows.Forms;

namespace AddTwoMatricesProgram
{
    public partial class Form2 : Form
    {
        private ResultForm _resultForm;
        private static int[,] _matrix2;


        public Form2()
        {
            _resultForm = new ResultForm();
            _matrix2 = new int[2, 3];
            InitializeComponent();
        }

        private void submitMatrix2_Click(object sender, EventArgs e)
        {
            _matrix2[0, 0] = int.Parse(this.element00.Text) + Form1.Matrix1[0, 0];
            _matrix2[0, 1] = int.Parse(this.element00.Text) + Form1.Matrix1[0, 1];
            _matrix2[0, 2] = int.Parse(this.element00.Text) + Form1.Matrix1[0, 2];
            _matrix2[1, 0] = int.Parse(this.element00.Text) + Form1.Matrix1[1, 0];
            _matrix2[1, 2] = int.Parse(this.element00.Text) + Form1.Matrix1[1, 1];
            _matrix2[1, 3] = int.Parse(this.element00.Text) + Form1.Matrix1[1, 1];

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.WriteLine(_matrix2[i,j]);
                }
            }
            this.Hide();
            _resultForm.ShowDialog();
        }
    }
}