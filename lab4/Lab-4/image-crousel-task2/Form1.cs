﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_crousel_task2
{
    public partial class Form1 : Form
    {
        private string[] images = new[] {"C:\\Users\\F 95\\RiderProjects\\Lab-4\\image-crousel-task2\\assets\\1.jpg",
                                        "C:\\Users\\F 95\\RiderProjects\\Lab-4\\image-crousel-task2\\assets\\2.jpg",
                                        "C:\\Users\\F 95\\RiderProjects\\Lab-4\\image-crousel-task2\\assets\\3.jpg"};
        private int count = 0;
        public Form1()
        {
            InitializeComponent();
            this.picSlideShow.ImageLocation = images[0];
            count++;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (count != 0)
            {
                this.picSlideShow.Image = Image.FromFile(images[count--]);
            }
            else
            {
                return;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (count != 2)
            {
                this.picSlideShow.Image = Image.FromFile(images[count++]);
            }
            else
            {
                return;
            }  
        }
    }
}