﻿using System;

namespace lab2_3
{
    internal class InClassName
    {
        public InClassName(int b)
        {
            B = b;
        }

        public int B { get; private set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            parameterRefactoring(2, new InClassName(2));
        }

        static void parameterRefactoring(int a, InClassName inClassName)
        {
            
        }
    }
}