﻿using System;

namespace lab2_6
{
    internal interface IProgram
    {
    }

    class Program : IProgram
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}