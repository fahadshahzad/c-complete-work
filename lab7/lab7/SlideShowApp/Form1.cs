﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace SlideShowApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("D:\\");

            DirectoryInfo[] dirs = di.GetDirectories();

            foreach (DirectoryInfo item in dirs)
            {
                cmb.Items.Add(item.Name);

            }
            foreach (var item in di.GetFiles())
            {
                listBox1.Items.Add(item.Name);
            }
          
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.Items.Contains("*.jpg"))
            {
                pictureBox1.ImageLocation = "D:\\download.jpg";
            }
        }
    }
}
