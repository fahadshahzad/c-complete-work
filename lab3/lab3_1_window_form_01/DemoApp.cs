﻿using System;
using System.Windows.Forms;

namespace lab3_1_window_form_01
{
    public partial class DemoApp : Form
    {
        public DemoApp()
        {
            InitializeComponent();
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            this.btnDemo.Left = this.btnDemo.Left + 10;
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            this.btnDemo.Left = this.btnDemo.Left - 10;

        }

        private void btnTop_Click(object sender, EventArgs e)
        {
            this.btnDemo.Top = this.btnDemo.Top - 10;
        }

        private void btnBottom_Click(object sender, EventArgs e)
        {
            this.btnDemo.Top = this.btnDemo.Top+10;
        }
    }
}