﻿using System;
using System.Windows.Forms;

namespace lab3_1_window_form_01
{
    public partial class calculator : Form
    {
        public calculator()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(this.number1.Text);
            int num2 = int.Parse(this.number2.Text);
            this.resultText.Text = (num1*num1+"");
        }

        private void resultText_TextChanged(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(this.number1.Text);
            int num2 = int.Parse(this.number2.Text);
            this.resultText.Text = (num1+num2+"");
        }

        private void btnSubtract_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(this.number1.Text);
            int num2 = int.Parse(this.number2.Text);
            this.resultText.Text = (num2+num1+"");
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(this.number1.Text);
            int num2 = int.Parse(this.number2.Text);
            this.resultText.Text = (num1/num2+"");
        }
    }
}