﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace lab3_1_window_form_01
{
    public partial class FullCalculator : Form
    {
        private int number1;
        private int number2;
        private int result;
        private string operatorBtn;
        public FullCalculator()
        {
            InitializeComponent();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="0";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.textBox.Text = "";
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            string str = this.textBox.Text;
            String[] splited = str.Split(" ");
            
            this.number2 = int.Parse(splited[splited.Length-1]);
            
            if(this.operatorBtn=="+")
                this.result = number1 + number2;
            else if(this.operatorBtn=="-")
                this.result = number1 - number2;
            else if(this.operatorBtn=="*")
                this.result = number1 * number2;
            else if(this.operatorBtn=="/")
                this.result = number1 / number2;

            this.textBox.Text = result+"";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            this.textBox.Text+="9";
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            this.number1 = int.Parse(this.textBox.Text);
            this.textBox.Text += " + ";
            this.operatorBtn = "+";
        }

        private void btnSubtract_Click(object sender, EventArgs e)
        {
            this.number1 = int.Parse(this.textBox.Text);

            this.textBox.Text += " - ";

            this.operatorBtn = "-";
        }

        private void btnMultiply_Click(object sender, EventArgs e)
        {
            this.number1 = int.Parse(this.textBox.Text);
            this.textBox.Text += " * ";
            this.operatorBtn = "*";
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            this.number1 = int.Parse(this.textBox.Text);
            this.textBox.Text += " / ";
            this.operatorBtn = "/";
        }
    }
}