﻿namespace lab3_1_window_form_01
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.enter = new System.Windows.Forms.Label();
            this.textMonth = new System.Windows.Forms.TextBox();
            this.btnSesson = new System.Windows.Forms.Button();
            this.seasonName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // enter
            // 
            this.enter.Location = new System.Drawing.Point(36, 45);
            this.enter.Name = "enter";
            this.enter.Size = new System.Drawing.Size(76, 17);
            this.enter.TabIndex = 2;
            this.enter.Text = "Enter Month";
            // 
            // textMonth
            // 
            this.textMonth.Location = new System.Drawing.Point(138, 42);
            this.textMonth.Name = "textMonth";
            this.textMonth.Size = new System.Drawing.Size(256, 20);
            this.textMonth.TabIndex = 3;
            // 
            // btnSesson
            // 
            this.btnSesson.Location = new System.Drawing.Point(151, 80);
            this.btnSesson.Name = "btnSesson";
            this.btnSesson.Size = new System.Drawing.Size(115, 32);
            this.btnSesson.TabIndex = 4;
            this.btnSesson.Text = "Find Season";
            this.btnSesson.UseVisualStyleBackColor = true;
            this.btnSesson.Click += new System.EventHandler(this.btnSesson_Click);
            // 
            // seasonName
            // 
            this.seasonName.Location = new System.Drawing.Point(138, 141);
            this.seasonName.Name = "seasonName";
            this.seasonName.Size = new System.Drawing.Size(226, 32);
            this.seasonName.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.seasonName);
            this.Controls.Add(this.btnSesson);
            this.Controls.Add(this.textMonth);
            this.Controls.Add(this.enter);
            this.Name = "Form1";
            this.Text = "Home Page";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Label seasonName;

        private System.Windows.Forms.Button btnSesson;
        private System.Windows.Forms.TextBox textMonth;

        private System.Windows.Forms.Label enter;

        #endregion
    }
}

