﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tic_tac_toe_game
{
    public partial class Form1 : Form
    {
        private static string[] _options = new[] {"X", "O"};

        public Form1()
        {
            InitializeComponent();
        }

        public int Iterations { get; set; } = 0;

        public bool UserIteration { get; set; } = true;

        public int UserCount { get; set; } = 0;

        public int MachineCount { get; set; } = 0;
        
        public void setUserWins()
        {
            this.btnOne.Text = "O";
            this.btnTwo.Text = "O";
            this.btnThree.Text = "O";
            this.btnFour.Text = "O";
            this.btnFive.Text = "O";
            this.btnSix.Text = "O";
            this.btnSeven.Text = "O";
            this.btnEight.Text = "O";
        }
        private void btnPlayAgain_Click(object sender, EventArgs e)
        {
            this.lblWinner.Text = "Winner is ________";
            this.btnOne.Text = "";
            this.btnTwo.Text = "";
            this.btnThree.Text = "";
            this.btnFour.Text = "";
            this.btnFive.Text = "";
            this.btnSix.Text = "";
            this.btnSeven.Text = "";
            this.btnEight.Text = "";
        }

        private void btnSeven_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int userGuess  = rnd.Next(0, 2);
            this.btnSeven.Text = _options[userGuess];
            UserIteration = false;
            this.btnPlayAgain.Hide();
            
            //machine ki bare hai
            int machineGuess  = rnd.Next(0, 2);
            this.btnEight.Text = _options[machineGuess];
            this.btnPlayAgain.Show();
            
            if (btnOne.Text=="O" && btnFour.Text=="O" && btnSeven.Text=="O")
            {
                this.lblWinner.Text = "User Wins";
                setUserWins();
            } 
            if (btnOne.Text=="X" && btnFour.Text=="X" && btnSeven.Text=="X")
            {
                this.lblWinner.Text = "Machine Wins";
              //  setMachineWins();
            }
            if (btnTwo.Text=="O" && btnFive.Text=="O" && btnEight.Text=="O")
            {
                this.lblWinner.Text = "User Wins";
                // setUserWins();
            } 
            if (btnTwo.Text=="X" && btnFive.Text=="X" && btnEight.Text=="X")
            {
                this.lblWinner.Text = "Machine Wins";
                //  setMachineWins();
            }

        }

        private void btnOne_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int userGuess  = rnd.Next(0, 2);
            this.btnOne.Text = _options[userGuess];
            UserIteration = false;
            this.btnPlayAgain.Hide();
  
            //machine ki bare hai
            int machineGuess  = rnd.Next(0, 2);
            this.btnTwo.Text = _options[machineGuess];
            this.btnPlayAgain.Show();
        }

        private void btnThree_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int userGuess  = rnd.Next(0, 2);
            this.btnThree.Text = _options[userGuess];
            UserIteration = false;
            this.btnPlayAgain.Hide();

            //machine ki bare hai
            int machineGuess  = rnd.Next(0, 2);
            this.btnFour.Text = _options[machineGuess];
  
            this.btnPlayAgain.Show();
            //check if any winner exists
            if (btnOne.Text=="O" && btnTwo.Text=="O" && btnThree.Text=="O")
            {
                this.lblWinner.Text = "User Wins";
                //      setUserWins();
            } 
            if (btnOne.Text=="X" && btnTwo.Text=="X" && btnThree.Text=="X")
            {
                this.lblWinner.Text = "Machine Wins";
                //   setMachineWins();
            }
            
            
            
        }

        private void btnFive_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int userGuess  = rnd.Next(0, 2);
            this.btnFive.Text = _options[userGuess];
            UserIteration = false;
            this.btnPlayAgain.Hide();

            //machine ki bare hai
            int machineGuess  = rnd.Next(0, 2);
            this.btnSix.Text = _options[machineGuess];
            this.btnPlayAgain.Show();
            
            if (btnFour.Text=="O" && btnFive.Text=="O" && btnSix.Text=="O")
            {
                this.lblWinner.Text = "User Wins";
            } 
            if (btnFour.Text=="X" && btnFive.Text=="X" && btnSix.Text=="X")
            {
                this.lblWinner.Text = "Machine Wins";
            }

        }

        private void btnNine_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnTwo_Click(object sender, EventArgs e)
        {
            btnOne_Click(sender, e);
        }

        private void btnFour_Click(object sender, EventArgs e)
        {
            btnThree_Click(sender: sender, e: e);
        }

        private void btnSix_Click(object sender, EventArgs e)
        {
            btnFive_Click(sender: sender, e: e);
        }

        private void btnEight_Click(object sender, EventArgs e)
        {
            btnSeven_Click(sender: sender, e: e);
        }
    }
}