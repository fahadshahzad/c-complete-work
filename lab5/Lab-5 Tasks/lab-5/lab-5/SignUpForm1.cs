﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab_5
{
    public partial class SignUpForm1 : Form
    {
        private string username1;
        private string password1;
        private List<String> contactList;
        
        private string contactNumber1;
        private string contactNumber2;

        private int lblTopMargin = 230;
        private int lblLeftMargin = 30;

        private string[] lblContactList = new string[] {"Enter Contact No: 2", "Enter Contact No: 3"};
        private TextBox[] numberBox;
        private Button addMoreButton;

        public SignUpForm1()
        {
            InitializeComponent();
            contactList = new List<string>();
            numberBox = new TextBox[2];
            addMoreButton = new Button();
            addMoreButton.Text = "Add More";
        }

        public int Counter { get; set; } = 0;

        public string Username1
        {
            get => username1;
            set => username1 = value;
        }

        public string Password1
        {
            get => password1;
            set => password1 = value;
        }

        public string ContactNumber1
        {
            get => contactNumber1;
            set => contactNumber1 = value;
        }

        public string ContactNumber2
        {
            get => contactNumber2;
            set => contactNumber2 = value;
        }
        
        

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.username.Text = "";
            this.password.Text = "";
            this.contactNo1.Text = "";
            foreach (var numBox in numberBox)
            {
                numBox.Text = "";
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Username1 = this.username.Text;
            Password1 = this.password.Text;
            for (int i = 0; i < numberBox.Length; i++)
            {
                if (numberBox[i]!=null)
                {
                    contactList.Add(this.numberBox[i].Text);
                }
            }
            
            Console.WriteLine(Username1+"\n"+Password1);
            foreach (var contact in contactList)
            {
                Console.WriteLine("Contact: "+contact);
            }
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (Counter==0)
            {
                Label contact2 = new Label();
                contact2.Text = lblContactList[Counter];
                contact2.Name = "contact2";
                contact2.Left = lblLeftMargin;
                contact2.Top = lblTopMargin;

                TextBox txtNumber2 = new TextBox();
                numberBox[Counter] = txtNumber2;
                numberBox[Counter].Name= "contact1";
                numberBox[Counter].Width= 230;
                numberBox[Counter].Left= 240;
                numberBox[Counter].Top = 230;
                this.pictureBox1.Top = 230;
                this.btnClear.Top = 280;
                this.btnSubmit.Top = 280;

                this.Controls.Add(contact2);
                this.Controls.Add(numberBox[Counter]);
                Counter++;
            }else if (Counter==1)
            {
                Label contact3 = new Label();
                contact3.Text = lblContactList[Counter];
                contact3.Name = "contact3";
                contact3.Left = lblLeftMargin;
                contact3.Top = lblTopMargin+30;

                TextBox txtNumber3 = new TextBox();
                numberBox[Counter] = txtNumber3 ;
                numberBox[Counter].Name= "contact1";
                numberBox[Counter].Width= 200;
                numberBox[Counter].Height=20;
                numberBox[Counter].Left= 240;
                numberBox[Counter].Top = 260;
                this.pictureBox1.Top = 260;
                this.btnClear.Top = 300;
                this.btnSubmit.Top = 300;

                this.Controls.Add(contact3);
                this.Controls.Add(numberBox[Counter]);
                Counter++;
                this.pictureBox1.Hide();//means all contact are done
            }
        }
    }
}