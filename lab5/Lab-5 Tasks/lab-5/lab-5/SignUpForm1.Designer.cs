﻿using System.ComponentModel;

namespace lab_5
{
    partial class SignUpForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignUpForm1));
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblContactNo1 = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.contactNo1 = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUsername
            // 
            this.lblUsername.Location = new System.Drawing.Point(42, 46);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(150, 26);
            this.lblUsername.TabIndex = 0;
            this.lblUsername.Text = "Enter username";
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(42, 98);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(150, 26);
            this.lblPassword.TabIndex = 1;
            this.lblPassword.Text = "Enter Password";
            // 
            // lblContactNo1
            // 
            this.lblContactNo1.Location = new System.Drawing.Point(42, 155);
            this.lblContactNo1.Name = "lblContactNo1";
            this.lblContactNo1.Size = new System.Drawing.Size(150, 26);
            this.lblContactNo1.TabIndex = 2;
            this.lblContactNo1.Text = "Enter Contact No# 01\r\n";
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(185, 46);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(229, 20);
            this.username.TabIndex = 3;
            // 
            // contactNo1
            // 
            this.contactNo1.Location = new System.Drawing.Point(185, 155);
            this.contactNo1.Name = "contactNo1";
            this.contactNo1.Size = new System.Drawing.Size(229, 20);
            this.contactNo1.TabIndex = 4;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(185, 104);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(229, 20);
            this.password.TabIndex = 5;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(185, 235);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(125, 22);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear\r\n";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(334, 235);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(80, 22);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = ((System.Drawing.Image) (resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image) (resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(442, 155);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 20);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // SignUpForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.password);
            this.Controls.Add(this.contactNo1);
            this.Controls.Add(this.username);
            this.Controls.Add(this.lblContactNo1);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUsername);
            this.Name = "SignUpForm1";
            this.Text = "SignUpForm1";
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.PictureBox pictureBox1;

        private System.Windows.Forms.Button btnSubmit;

        private System.Windows.Forms.Button btnClear;

        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox contactNo1;
        private System.Windows.Forms.TextBox password;

        private System.Windows.Forms.Label lblContactNo1;

        private System.Windows.Forms.Label lblPassword;

        private System.Windows.Forms.Label lblUsername;

        #endregion
    }
}