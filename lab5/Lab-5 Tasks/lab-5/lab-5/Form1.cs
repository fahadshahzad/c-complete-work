﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace lab_5
{
    public partial class Form1 : Form
    {
        private string[] images = new[] {"C:\\Users\\F 95\\RiderProjects\\lab-5\\lab-5\\assets\\1.jpg",
                                         "C:\\Users\\F 95\\RiderProjects\\lab-5\\lab-5\\assets\\2.jpg",
                                         "C:\\Users\\F 95\\RiderProjects\\lab-5\\lab-5\\assets\\3.jpg",
                                         "C:\\Users\\F 95\\RiderProjects\\lab-5\\lab-5\\assets\\4.jpg"};
        private int count = 0;
        public Form1() 
        {
            InitializeComponent();
            this.picSlideShow.ImageLocation = images[0];
            count++;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (count == 0) count = images.Length;
            this.picSlideShow.Image = Image.FromFile(images[count--]);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (count == images.Length) count = 0;
            this.picSlideShow.Image = Image.FromFile(images[count++]);
        }

        private void btnPlay_Click(object sender, EventArgs e)
        { 
            timer.Start();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            timer.Stop();
            Console.WriteLine("Timer stoped");
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (count == images.Length) count = 0;
            picSlideShow.ImageLocation = images[count++];
            if (btnReversePlay.IsMirrored)
            {
                if (count == 0) count = images.Length;
                picSlideShow.ImageLocation = images[count--];
            }
        }

        private void btnReversePlay_Click(object sender, EventArgs e)
        {
            timer.Start();
        }
    }
}
