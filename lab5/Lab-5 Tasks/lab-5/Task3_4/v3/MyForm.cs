using System;
using System.Windows.Forms;
namespace MyNameSpace{
	public class MyForm: Form{
		private Button btnLoad;
		private PictureBox pboxPhoto;
		public MyForm(){
			this.Text = "My Form";
			btnLoad=new Button();
			btnLoad.Text= "&Load";
			btnLoad.Width= this.Width/2;
			btnLoad.Height=20;
			btnLoad.Left= (this.Width-btnLoad.Width)/2;
			btnLoad.Top = 50;
			btnLoad.UseMnemonic = true;
			btnLoad.Click +=new System.EventHandler(this.OnLoadClick);
			pboxPhoto = new PictureBox();
			pboxPhoto.BorderStyle = BorderStyle.Fixed3D;
			pboxPhoto.Width = this.Width/2;
			pboxPhoto.Height = this.Height/2;
			pboxPhoto.Left = (this.Width - pboxPhoto.Width)/2;
			pboxPhoto.Top = (this.Height - pboxPhoto.Height)/2;
			
			//adding controlls on form
			this.Controls.Add(btnLoad);
			this.Controls.Add(pboxPhoto);
		}
		private void OnLoadClick(object sender, System.EventArgs e){
			this.pboxPhoto.ImageLocation = @"C:\\Users\\F 95\\RiderProjects\\lab-5\\lab-5\\assets\\1.jpg";
		}
		public static void Main(string[] args){
			Application.Run(new MyForm());
		}
	}
}