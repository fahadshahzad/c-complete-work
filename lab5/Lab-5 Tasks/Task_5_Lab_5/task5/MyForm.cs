﻿using System.Windows.Forms;

namespace task5
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();
        }

        public static void Main(string[] args)
        {
            Application.Run(new MyForm());
        }
    }
}