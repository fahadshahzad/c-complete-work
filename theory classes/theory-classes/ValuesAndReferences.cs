﻿using System;

namespace theory_classes
{
    public class ValuesAndReferences
    {
        public void ChangeValueType(ref int a){
            a = 5;
        }

        public void MethodOut(out int number)
        {
            number = 12;
        }
        
        //out keyword and returning the multiple values in void method
        public void ReturnMultipleValuesWithOut(out int number1, out string name,out Boolean status)
        {
            number1 = 100;
            name = "Fahad Shahzad";
            status = true;

        }
    }
}