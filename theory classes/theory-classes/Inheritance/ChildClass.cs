﻿using System;

namespace theory_classes.Inheritance
{
    public class ChildClass: InheritanceParentClass
    {
        public void ChildClassMethod()
        {
            Console.WriteLine("This is child class method");
        }

        public void UseInternalName()
        {
            Console.WriteLine(Name);
        }

        public void UseProtectedMember()
        {
            Console.WriteLine(ProtectedName);
        }

    }
}