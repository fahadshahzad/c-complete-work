﻿using System;

namespace theory_classes.Inheritance
{
    public class InheritanceParentClass
    {
        internal string Name = "Internal Name";
        protected string ProtectedName = "Protected name";  
        public void StatusClass()
        {
            Console.WriteLine("Parent class");
        }
    }
}