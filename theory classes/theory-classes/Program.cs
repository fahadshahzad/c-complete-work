﻿using System;
using System.Threading;
using theory_classes.Inheritance;

namespace theory_classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Conditions conditions = new Conditions();
            conditions.CheckEvenOdd(number:1);
            conditions.TestSwithcCase(choice:2);
            conditions.TestLoops(limit:5);


            ValuesAndReferences valuesAndReferences = new ValuesAndReferences();
            int b = 10;
            valuesAndReferences.ChangeValueType(ref b);
            Console.WriteLine("Change Value by Reference = "+b);
            
            valuesAndReferences.MethodOut(out b);
            Console.WriteLine(b);

            int numberOut = 12;
            String name = "fahad shahzad";
            Boolean status = false;
            valuesAndReferences.ReturnMultipleValuesWithOut(out numberOut, out name, out status);
            Console.WriteLine("Number by Out = "+numberOut+"\nName: "+name+"\nStatus = "+status);

            Console.WriteLine("__________________________________________");
            NamedArguments namedArguments = new NamedArguments();
            namedArguments.CheckNamedArguments(requiredNumber:12, nameOptional:"Muhammad Fahad Shahzad");
            Console.WriteLine(namedArguments.CalculatorBMI(48,6));
            
            Console.WriteLine("__________________Test Params________________________");
            ClassWithParams classWithParams = new ClassWithParams();
            classWithParams.CheckParams(12,3,4,2,1,1,1);
            
            Console.WriteLine("__________________Test Outer class Access modifiers________________________");
            AccessModifiers accessModifiers = new AccessModifiers();
            //no access of default and private
            Console.WriteLine("__________________Test Inner class Access Modifiers________________________");
            AccessModifierInnerClass accessModifierInnerClass = new AccessModifierInnerClass();
            //accessModifierInnerClass. no access of private and default
            
            Console.WriteLine("__________________Test of Internal________________________");
            InternalMembersTest internalMembersTest = new InternalMembersTest();
            Console.WriteLine(internalMembersTest.Name);

            Console.WriteLine("__________________Inheritance Test________________________");
            ChildClass childClass = new ChildClass();
            childClass.StatusClass();
            childClass.ChildClassMethod();
            childClass.UseInternalName();
            childClass.UseProtectedMember();
            
        }
    }

    class AccessModifierInnerClass
    {
        private int _number2 = 121;
        double _number = 12.3;
    }   

}