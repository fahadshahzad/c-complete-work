﻿using System;

namespace theory_classes
{
    public class Conditions
    {
        public void CheckEvenOdd(int number)
        {
            if (number%2==0)
            {
                Console.WriteLine("Number is even");
            }
            else if (number == 0)
            {
                Console.WriteLine("Number is 0");
            }
            else
            {
                Console.WriteLine("number is odd");
            }
        }

        public void TestSwithcCase(int choice)
        {
            switch (choice)
            {
                case 1:
                    Console.WriteLine("number is one");
                    break;
                case 2:
                    Console.WriteLine("number is 2");
                    break;
                default:
                    Console.WriteLine("Number is not allowed");
                    break;
            }
        }
        public void TestLoops(int limit){
            for (int j = 0; j < limit; j++)
            {
                Console.WriteLine(j);
            }

            while (limit!=10)
            {
                Console.WriteLine(limit);
                limit += 1;
            }
        }
    }
}