﻿using System;

namespace theory_classes
{
    public class ClassWithParams
    {
        public void CheckParams(params int[] number)
        {
            foreach (var num in number)
            {
                Console.WriteLine(num);
            }
        }
    }
}