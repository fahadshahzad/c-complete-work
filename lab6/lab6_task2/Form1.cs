﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab6_task2
{
    public partial class frmSplashScreen : Form
    {
        public frmSplashScreen()
        {
            InitializeComponent();
        }
        int count = 1;
        private void frmSplashScreen_Load(object sender, EventArgs e)
        {
            this.progressBar1.Minimum = 1;
            this.progressBar1.Maximum = 100;
            this.progressBar1.Value = 1;
            timer1.Start();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            count++;

            this.progressBar1.Value = count * 10;     

            if (this.progressBar1.Value == 100)
            {
                timer1.Stop();
                this.Hide();
                frmLogin login = new frmLogin();
                login.ShowDialog();
                
               
            }
           
        }
    }
}
