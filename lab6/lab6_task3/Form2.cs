﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab6_task3
{
    public partial class Form2 : Form
    {
        string size,crust,takeOrEat;
        string []topping=new string[4];
        public Form2()
        {
            InitializeComponent();
        }

       

        public Form2(string size,string[] topping,string crust,string takeOrEat)
        {
            InitializeComponent();
            this.size = size;
            this.crust = crust;
            this.takeOrEat = takeOrEat;
            for (int i = 0; i < topping.Length; i++)
            {
                this.topping[i] = topping[i];
            }
         
        }
        public void Form2_Load(object sender, EventArgs e)
        {
            lblSize.Text += " " + size;
            for (int i = 0; i < topping.Length; i++)
            {
                lblTopping.Text += " " + topping[i];
            }
            lblCrust.Text += crust;
            label3.Text += " " + takeOrEat;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }
    }
}
